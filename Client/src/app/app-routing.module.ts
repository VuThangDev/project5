import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './users/auth/auth.guard';
import { BasicinfoComponent } from './users/component/basicinfo/basicinfo.component';
import { CheckzoneComponent } from './users/component/checkzone/checkzone.component';
import { SigncontractComponent } from './users/component/signcontract/signcontract.component';
import { ThanhtoanComponent } from './users/component/thanhtoan/thanhtoan.component';
import { BannerRegisterShopComponent } from './users/page/banner-register-shop/banner-register-shop.component';
import { DrinkComponent } from './users/page/drink/drink.component';
import { FreshComponent } from './users/page/fresh/fresh.component';
import { HistoryOrderComponent } from './users/page/history-order/history-order.component';
import { HomeComponent } from './users/page/home/home.component';
import { ListrestaurantFindComponent } from './users/page/listrestaurant-find/listrestaurant-find.component';
import { LoginComponent } from './users/page/login/login.component';
import { MerchantComponent } from './users/page/merchant/merchant.component';
import { QLDHComponent } from './users/page/qldh/qldh.component';
import { QLSPComponent } from './users/page/qlsp/qlsp.component';
import { RegisterDriverComponent } from './users/page/register-driver/register-driver.component';
import { RegisterShopComponent } from './users/page/register-shop/register-shop.component';
import { ShopComponent } from './users/page/shop/shop.component';
import { UserComponent } from './users/user/user.component';

const routes: Routes = [
  {path:'', redirectTo: "/home", pathMatch: 'full'},
  {path:'home', component: UserComponent, children:[
    {path:'', component: HomeComponent},
    {path:'fresh', component: FreshComponent},
    {path:'drink', component: DrinkComponent},
    {path:'shop/:id', component: ShopComponent},
    {path:'danh-sach-dia-diem/:ten', component: ListrestaurantFindComponent},
    {path:'history', component: HistoryOrderComponent},
    {path:'register', component: BannerRegisterShopComponent}
  ]}
  ,
  {path: 'merchant', component: MerchantComponent, canActivate:[AuthGuard], children:[
    {path: 'order', component: QLDHComponent},
    {path: 'category', component: QLSPComponent}
  ]},
  {path: 'login', component: LoginComponent},
  {path: 'registerdriver', component: RegisterDriverComponent, canActivate:[AuthGuard]},
  {path: 'register', component: RegisterShopComponent, canActivate:[AuthGuard], children:
  [
    {path: 'checkzone', component: CheckzoneComponent},
    {path: 'signcontract', component: SigncontractComponent},
    {path: 'basicinfo', component: BasicinfoComponent}
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
