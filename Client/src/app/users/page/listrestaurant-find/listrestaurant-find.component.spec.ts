import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListrestaurantFindComponent } from './listrestaurant-find.component';

describe('ListrestaurantFindComponent', () => {
  let component: ListrestaurantFindComponent;
  let fixture: ComponentFixture<ListrestaurantFindComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListrestaurantFindComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListrestaurantFindComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
