import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Restaurants } from '../../shared/restaurants.model';

@Component({
  selector: 'app-listrestaurant-find',
  templateUrl: './listrestaurant-find.component.html',
  styleUrls: ['./listrestaurant-find.component.css']
})
export class ListrestaurantFindComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }

  namef:any = this.route.snapshot.paramMap.get('ten')
  restaurant: Restaurants []
  lengthp: number 
  sizep:number = 10
  indexp:number = 0

  ngOnInit(): void {
    this.loadslfind()
    this.loadfind()    
  }


  async loadfind() {
    await fetch(`http://localhost:24454/api/Shops/GetShopsName`,
      {
        headers: {
          'Accept': 'text/plain',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({namf: this.namef, size: this.sizep, index: this.indexp * this.sizep})
      })
      .then(response => response.json())
      .then(data =>{        
        this.restaurant = data
      })
      .catch(function (res) { console.log(res) })
  }



  OnchangePage(event: PageEvent) {    
    this.lengthp = event.length
    this.sizep = event.pageSize
    this.indexp = event.pageIndex
    this.loadfind()
  }


  async loadslfind() {
    await fetch(`http://localhost:24454/api/Shops/GetslShopsName`,
      {
        headers: {
          'Accept': 'text/plain',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({namf: this.namef, size: this.sizep, index: this.indexp * this.sizep})
      })
      .then(response => response.json())
      .then(data =>{
        this.lengthp = data.length        
      })
      .catch(function (res) { console.log(res) })
  }
}
