import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerRegisterShopComponent } from './banner-register-shop.component';

describe('BannerRegisterShopComponent', () => {
  let component: BannerRegisterShopComponent;
  let fixture: ComponentFixture<BannerRegisterShopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BannerRegisterShopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerRegisterShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
