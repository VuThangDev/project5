import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { RestaurantsService } from '../../shared/restaurants.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private sevice: RestaurantsService, private route: Router) { }
  formmodel={
    Email:'',
    Password: ''
  }
  ngOnInit(): void {
  }


  login() {
    // console.log(form.value);
    this.sevice.login(this.formmodel).subscribe(
      (res: any) => {            
        
        if(res.user[0].role == 1){
          console.log(res)
          localStorage.setItem('token',res.token)
          localStorage.setItem('id',res.user[0].idUser)
          history.back()
        }else if(res.user[0].role == 2){
          console.log(res.user[0].idUser)
          localStorage.setItem('token',res.token)
          localStorage.setItem('ids', res.user[0].idUser)
          this.route.navigate(['/merchant'])
        }    
      },
      (err: any) => {
        if (err.status == 400) {
          alert("Tài khoản hoặc mật khẩu không chính xác")
        }
      }
    )
  }

}
