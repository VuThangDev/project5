import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QLSPComponent } from './qlsp.component';

describe('QLSPComponent', () => {
  let component: QLSPComponent;
  let fixture: ComponentFixture<QLSPComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QLSPComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QLSPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
