import { Component, OnInit } from '@angular/core';
import { HttpClient, HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { Shop } from '../../shared/shop.model';

@Component({
  selector: 'app-merchant',
  templateUrl: './merchant.component.html',
  styleUrls: ['./merchant.component.css']
})
export class MerchantComponent implements OnInit {
  private _hubConnection: HubConnection;
  showimage: boolean = false;
  signaldata: any[] = [];

  shop:Shop 

  constructor() { }

  ngOnInit(): void {
    let id = localStorage.getItem('ids')
    this._hubConnection = new HubConnectionBuilder().withUrl('http://localhost:24454/notify').build();
    this._hubConnection.start()
      .then(() =>
        console.log('start'))
      .catch(err => {
        console.log('Error while establishing the connection')
      });

    this._hubConnection.on('BroadcastMessage', (message) => {
      if(message.idReciever = id){
        if(message.type != 1){
          alert(message.notice)
        }
        console.log(message)
        this.signaldata.push(message);
        this.showimage = true;
      }
    })

    this.getShop()
  }

  getShop(){
    let id = localStorage.getItem('ids')
    fetch(`http://localhost:24454/api/Shops/${id}`)
    .then(response => response.json())
    .then(data => this.shop = data)    
  }

  

}
