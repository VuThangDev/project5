import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QLDHComponent } from './qldh.component';

describe('QLDHComponent', () => {
  let component: QLDHComponent;
  let fixture: ComponentFixture<QLDHComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QLDHComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QLDHComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
