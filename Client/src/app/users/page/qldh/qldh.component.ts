import { StringMap } from '@angular/compiler/src/compiler_facade_interface';
import { Component, OnInit } from '@angular/core';
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

import { Order } from '../../shared/order.model';
const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
  { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
  { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
  { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
];
@Component({
  selector: 'app-qldh',
  templateUrl: './qldh.component.html',
  styleUrls: ['./qldh.component.css']
})
export class QLDHComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];

  data: any[] = [{
    position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'
  }];

  menu: any[]

  order: any[]

  nameShop: string = "acb"

  idorder: number
  iduser: number
  idshop: number
  idshiper: number
  mind: string
  status: number
  total: number

  id: number
  hiden: boolean = false
  constructor() { }

  ngOnInit(): void {
    this.getMenu()
  }


  getMenu() {

    let id = localStorage.getItem('ids')

    fetch(`http://localhost:24454/api/Shops/getordershop/${id}`)
      .then(response => response.json())
      .then(data => this.menu = data)
    // console.log("data",this.menu[0])
  }


  handleDescription(ids: any) {
    // let id = localStorage.getItem('id')\
    this.id = ids
    this.nameShop = this.menu[0].shops.name

    fetch(`http://localhost:24454/api/Shops/getordershopdetail/${ids}`)
      .then(response => response.json())
      .then(data => this.order = data)

    if (this.order) {
      this.hiden = true
    }
  }

  hidenoff() {
    this.hiden = false
  }


  confirm() {
    for (let i = 0; i < this.menu.length; i++) {
      if (this.menu[i].orders.idOrder == this.id) {
        this.idorder = this.menu[i].orders.idOrder
        this.iduser = this.menu[i].orders.idUser
        this.idshop = this.menu[i].orders.idShop
        this.idshiper = this.menu[i].orders.idShiper
        this.mind = this.menu[i].orders.mind
        this.status = 2
        this.total = this.menu[i].orders.total
      }
    }

    fetch(`http://localhost:24454/api/Shops/${this.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ "IdOrder": this.idorder, "IdUser": this.iduser, "IdShop": this.idshop, "IdShiper": this.idshiper, "Mind": this.mind, "Status": this.status, "Total": this.total, }),
    })

      .then(data => {
        if (data.status == 200) {
          this.notice(this.iduser, this.idshop, "Đơn hàng đã được xác nhận", 3)

          this.hiden = false

        }
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }

  reject() {
    for (let i = 0; i < this.menu.length; i++) {
      if (this.menu[i].orders.idOrder == this.id) {
        this.idorder = this.menu[i].orders.idOrder
        this.iduser = this.menu[i].orders.idUser
        this.idshop = this.menu[i].orders.idShop
        this.idshiper = this.menu[i].orders.idShiper
        this.mind = this.menu[i].orders.mind
        this.status = -1
        this.total = this.menu[i].orders.total
      }
    }

    fetch(`http://localhost:24454/api/Shops/${this.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ "IdOrder": this.idorder, "IdUser": this.iduser, "IdShop": this.idshop, "IdShiper": this.idshiper, "Mind": this.mind, "Status": this.status, "Total": this.total, }),
    })

      .then(data => {
        if (data.status == 200) {
          this.notice(this.iduser, this.idshop, "Đơn hàng không được xác nhận", 4)

          this.hiden = false

        }
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }

  notice(idu:number, ids:number, notice:string, type:number) {
    var data = {
      Type: type,
      Notice: notice,
      IdSender: ids,
      IdReciever: idu
    }
    fetch('http://localhost:24454/api/Message/noticeorderreciever', {
      method: 'POST', // or 'PUT'
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then(data => {
        // console.log('Success:', data);
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }
}

