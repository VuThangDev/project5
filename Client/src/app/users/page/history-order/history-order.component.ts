import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-history-order',
  templateUrl: './history-order.component.html',
  styleUrls: ['./history-order.component.css']
})
export class HistoryOrderComponent implements OnInit {

  constructor() { }

  menu: any[]

  order: any[]

  nameShop: string = "acb"

  hiden: boolean = false

  ngOnInit(): void {
    this.getMenu()
    // console.log("data",this.menu)
  }


  getMenu() {

    let id = localStorage.getItem('id')

    fetch(`http://localhost:24454/api/Menus/GetMenu/${id}`)
      .then(response => response.json())
      .then(data => this.menu = data)
    // console.log("data",this.menu[0])
  }


  handleDescription(id: any) {
    // let id = localStorage.getItem('id')\
    this.nameShop = this.menu[0].shops.name

    fetch(`http://localhost:24454/api/Menus/GetMenuUDetail/${id}`)
      .then(response => response.json())
      .then(data => this.order = data)

    if (this.order) {
      this.hiden = true
    }
    console.log("data", this.order)
  }

  hidenoff() {
    this.hiden = false
  }
}
