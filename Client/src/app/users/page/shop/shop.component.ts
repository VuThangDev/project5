import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Restaurants } from '../../shared/restaurants.model';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {

  restaurant:Restaurants  
  idshop:any

  constructor(private route : ActivatedRoute) { }

  ngOnInit(): void {
    this.getShop()    
  }

  async getShop(){
    let id = this.route.snapshot.paramMap.get('id')    
    this.idshop = id
    await fetch(`http://localhost:24454/api/Shops/${id}`)
      .then(response => response.json())
      .then(data => this.restaurant = data);  
    console.log(this.restaurant);  
  }
}
