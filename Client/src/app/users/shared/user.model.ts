export class User {
    idUser:number
    name:string
    email:string
    password:string
    phone:number
    address:string
    avatar:string
    Role:number
    lat:number
    lng:number
}
