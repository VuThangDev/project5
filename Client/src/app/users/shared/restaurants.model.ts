// import * as internal from "stream";

export class Restaurants {
    idShop:number;
    idUser:number;
    name:string="test";
    address:string="test";
    phone:number;
    type:number;
    avatar:string;
    rate:number;
    totalVote:number;
    createAt:string;
    lat:number;
    lng:number
}
