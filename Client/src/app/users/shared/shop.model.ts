export class Shop {
    address:string
    avatar:string
    city:number
    createAt:Date
    idShop:number
    idUser:number
    idUserNavigation:number
    lat:number
    lng:number
    name:string
    nameSearch:string
    phone:number
    rate:number
    selling:number
    totalVote:number
    type:number
}
