import { Injectable } from '@angular/core';

import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestaurantsService {

  constructor( private Http: HttpClient) { }

  baseURL:string = "http://localhost:24454/api"

  login(formdata:any){
    return this.Http.post(this.baseURL+'/Users/Login', formdata)
  }
}
