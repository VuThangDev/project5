import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfomationMainComponent } from './infomation-main.component';

describe('InfomationMainComponent', () => {
  let component: InfomationMainComponent;
  let fixture: ComponentFixture<InfomationMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfomationMainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfomationMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
