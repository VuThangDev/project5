import { Component, OnInit } from '@angular/core';
import { Restaurants } from '../../shared/restaurants.model';

@Component({
  selector: 'app-listrestaurant',
  templateUrl: './listrestaurant.component.html',
  styleUrls: ['./listrestaurant.component.css']
})
export class ListrestaurantComponent implements OnInit {
  restaurant: Restaurants[]
  constructor() { }

  ngOnInit(): void {
    this.loadRestaurantNear()
  }

  async loadRestaurantNear() {
    await fetch('http://localhost:24454/api/Shops/GetShopsNear/1')
      .then(response => response.json())
      .then(data => this.restaurant = data);    
  }

  async loadRestaurantSelling() {
    await fetch('http://localhost:24454/api/Shops/GetShopsSelling/1')
      .then(response => response.json())
      .then(data => this.restaurant = data);
  }

  async loadRestaurantRate() {
    await fetch('http://localhost:24454/api/Shops')
      .then(response => response.json())
      .then(data => this.restaurant = data);
  }


  async loadRestaurantTransport() {
    await fetch('http://localhost:24454/api/Shops')
      .then(response => response.json())
      .then(data => this.restaurant = data);
  }

  clickLoad() {
    let e = document.querySelector('.test mat-tab-header');
    let val: any = e?.attributes.getNamedItem("ng-reflect-selected-index")?.value;
    if(val == 0){
      this.loadRestaurantNear();
    }
    else if(val == 1){
      this.loadRestaurantSelling()
    }
    else if(val == 2){
      this.loadRestaurantRate()
    }
    else{
      this.loadRestaurantTransport()
    }    

  }


  test(id:any){
    console.log(id);
  }
}
