import { Component, EventEmitter, Input, OnInit, Output, SimpleChange, SimpleChanges } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { User } from '../../shared/user.model';
import { ThanhtoanComponent } from '../thanhtoan/thanhtoan.component';

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.css']
})
export class PayComponent implements OnInit {


  @Input() cart: any

  @Input() total: number

  @Output() test: EventEmitter<any> = new EventEmitter()  

  a: any[]

  user:User

  constructor( public dialog: MatDialog ) { }

  ngOnInit(): void {
    this.loaduser()
  }

  opendialog(): void {
    const dialogRef = this.dialog.open(ThanhtoanComponent, {
      data: this.cart,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');      
    });
  }

  delete(){
    this.cart = null
    this.total = 0
  }

  async loaduser() {
    let id = localStorage.getItem('id')
    await fetch(`http://localhost:24454/api/Users/${id}`)
      .then(response => response.json())
      .then(data => this.user = data);
  }

  sumPrice(): void {    
    this.total = 0
    for (let i = 0; i <= this.cart.length; i++) {
      this.total += (this.cart[i].price * this.cart[i].total)
    }
  }

  up(id: any) {
    for (let i = 0; i <= this.cart.length; i++) {
      if (this.cart[i].idDetail == id) {
        this.cart[i].total += 1
        this.sumPrice()
      }
    }


  }

  down(id: any) {
    for (let i = 0; i <= this.cart.length; i++) {
      if (this.cart[i].idDetail == id) {
        if (this.cart[i].total > 1) {
          this.cart[i].total -= 1
          this.sumPrice()
        }
        else {
          break
        }
      }
    }
  }

}
