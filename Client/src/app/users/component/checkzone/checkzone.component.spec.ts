import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckzoneComponent } from './checkzone.component';

describe('CheckzoneComponent', () => {
  let component: CheckzoneComponent;
  let fixture: ComponentFixture<CheckzoneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckzoneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckzoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
