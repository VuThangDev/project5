import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainMainHomeComponent } from './contain-main-home.component';

describe('ContainMainHomeComponent', () => {
  let component: ContainMainHomeComponent;
  let fixture: ComponentFixture<ContainMainHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContainMainHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainMainHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
