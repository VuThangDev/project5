import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SigncontractComponent } from './signcontract.component';

describe('SigncontractComponent', () => {
  let component: SigncontractComponent;
  let fixture: ComponentFixture<SigncontractComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SigncontractComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SigncontractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
