import { Component, HostListener, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Menu } from '../../shared/menu.model';
import { Menudetail } from '../../shared/menudetail.model';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  @Input() ids:any


  menus:Menu[]
  menudeatils:Menudetail[]
  text:string

  menupay:any[] =[]

  total:number = 0

  constructor(private route: Router) { }

  ngOnInit(): void {
    this.text="test communicated"
    this.loadMenu()
    this.loadMenuDetail()
  }


  @HostListener ('scroll', ['$event']) 

  onScroll(event:any) {
    console.log(event.target.scrollTop);
  }


  async loadMenu() {
    await fetch(`http://localhost:24454/api/Menus/GetMenuShop/${this.ids}`)
      .then(response => response.json())
      .then(data => this.menus = data);
  }

  async loadMenuDetail() {
    await fetch(`http://localhost:24454/api/Menus/GetDetailMenu/${this.ids}`)
      .then(response => response.json())
      .then(data => this.menudeatils = data);
  }

  test(id:any){
    let token = localStorage.getItem("token")
    if(token != null){
      let detail = this.menudeatils.find((val)=>{
        return val.idDetail == id
      })  
      
      let s = this.menupay.find((val)=>{
        return val.idDetail == id
      })  
  
      if(s != null){
        // const new_obj = { ...detail, total: 2, price: detail?.price }
        // this.menupay.push(new_obj)
        for(let i=0; i<=this.menupay.length; i++){        
          if(this.menupay[i].idDetail == detail?.idDetail){
            this.menupay[i].total += 1;          
            break
          }
        }
      }
      else{
        const new_obj = { ...detail, total: 1 }
        this.menupay.push(new_obj)
      }        
  
      let t = this.menupay.reduce((t, p)=>{
        return t + (p.price * p.total)
      }, 0)
  
      this.total = t
    }
    else{
      this.route.navigateByUrl('login')
    }
  } 
}
