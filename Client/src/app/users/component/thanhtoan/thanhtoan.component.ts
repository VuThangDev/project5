import { Component, Inject, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Menudetail } from '../../shared/menudetail.model';
import { Order } from '../../shared/order.model';
import { Restaurants } from '../../shared/restaurants.model';
import { User } from '../../shared/user.model';

@Component({
  selector: 'app-thanhtoan',
  templateUrl: './thanhtoan.component.html',
  styleUrls: ['./thanhtoan.component.css']
})
export class ThanhtoanComponent implements OnInit {

  menus: Menudetail []
  restaurant: Restaurants
  total:number
  sl:number
  user:User
  Distance:number
  price_ship:number = 0
  idorder:number

  objectDetail:any = {
    IdOrder: null,
    IdMenu: null,
    IdMenuDetail: null,
    Quantity: null
  }

  orderdetail:Order [] = []

  constructor(public dialogRef: MatDialogRef<ThanhtoanComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Menudetail [], private route: Router) { }

  ngOnInit(): void {
    console.log(localStorage.getItem('id'));
    this.loaduser()
    this.getShop()    
  }

  getDistance(){
    console.log("object");
    let R = 6378137; // Earth’s mean radius in meter
    let dLat = this.PI(this.restaurant.lat - this.user.lat);
    let dLong = this.PI(this.restaurant.lng - this.user.lng);
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(this.PI(this.user.lat)) * Math.cos(this.PI(this.restaurant.lat)) *
    Math.sin(dLong / 2) * Math.sin(dLong / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;
    this.Distance = Math.ceil(d/1000)
    console.log(this.Distance);
  }

  PI(x:number){
    return x * Math.PI / 180;
  }
  ship(){
    if(this.Distance<=5){
      this.price_ship = 20000
    }
    else if(this.Distance>5 && this.Distance <=15){
      this.price_ship = 35000
    }
    else if(this.Distance >15 && this.Distance <=30){
      this.price_ship = 50000
    }
    else{
      this.price_ship = 100000
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  async loaduser() {
    let id = localStorage.getItem('id')
    await fetch(`http://localhost:24454/api/Users/${id}`)
      .then(response => response.json())
      .then(data => this.user = data);
    console.log(this.user);
  }

  load(){
    this.total = 0
    this.sl = 0
    for(let i = 0; i<this.data.length; i++){
      this.total += (this.data[i].price * this.data[i].total)
      this.sl += this.data[i].total
    }
    this.total += this.price_ship
  }
  async getShop(){    
    await fetch(`http://localhost:24454/api/Shops/${this.data[0].idshop}`)
      .then(response => response.json())
      .then(data => this.restaurant = data);  
    this.getDistance()
    this.ship()
    this.load()
  }

  async pay(){    
    
    await fetch(`http://localhost:24454/api/Menus/postorder`,
      {
        headers: {
          'Accept': 'text/plain',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({IdUser: this.user.idUser, IdShop: this.restaurant.idShop, IdShiper: null, Status: 1, Total: this.total})
      })
      .then(response => response.json())
      .then(data =>{        
        let l = data.length
        this.idorder = data[l-1].idOrder
        for(let i = 0; i< this.data.length; i++){
              
          this.orderdetail.push({
            IdOrder: this.idorder,
            IdMenu: this.data[i].idMenu,
            IdMenuDetail: this.data[i].idDetail,
            Quantity: this.data[i].total
          })                
        }        
      })
      .catch(function (res) { console.log(res) })
      console.log("dâta",this.data);
      
      console.log("dâta",this.orderdetail);
      await fetch(`http://localhost:24454/api/Menus/postorderdetail`,
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify(this.orderdetail)
      })
      .then(response => response.json())
      .then(data =>{                
        if(data){
          this.noticeShop()
          document.querySelector('.success')?.classList.toggle('show')
          setTimeout(() => {
            document.querySelector('.success')?.classList.toggle('show')
            this.onNoClick()
          }, 3000);
          
        }
      })
      .catch(function (res) { console.log(res) })
  }

  noticeShop(){
    var data = {
      Type: 2,
      Notice: `Có đơn hàng mới`,
      IdSender: this.user.idUser,
      IdReciever: this.restaurant.idShop
    }
    fetch('http://localhost:24454/api/Message/noticeorder', {
      method: 'POST', // or 'PUT'
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then(data => {
        // console.log('Success:', data);
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }
}
