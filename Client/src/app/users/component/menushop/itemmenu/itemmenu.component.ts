import { Component, HostListener, OnInit } from '@angular/core';
import { Input } from '@angular/core';

@Component({
  selector: 'app-itemmenu',
  templateUrl: './itemmenu.component.html',
  styleUrls: ['./itemmenu.component.css']
})
export class ItemmenuComponent implements OnInit {

  @Input() text:string

  constructor() { }

  ngOnInit(): void {
    
  }

}
