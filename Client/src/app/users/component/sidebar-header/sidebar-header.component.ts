import { Component, OnInit } from '@angular/core';
import { HttpClient, HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { User } from '../../shared/user.model';

@Component({
  selector: 'app-sidebar-header',
  templateUrl: './sidebar-header.component.html',
  styleUrls: ['./sidebar-header.component.css']
})
export class SidebarHeaderComponent implements OnInit {
  foods: any[] = [
    { value: '1', viewValue: 'Hà Nội' },
    { value: '2', viewValue: 'TP.Hồ Chí Minh' },
    { value: '3', viewValue: 'Hưng Yên' },
    { value: '4', viewValue: 'Hải Dương' },
    { value: '5', viewValue: 'Hải Phòng' },
  ];

  private _hubConnection: HubConnection;
  showimage: boolean = false;
  signaldata: any[] = [];
  user: User

  selectedValue: number
  defaulVal: string = "Ha Noi"
  constructor() { }

  ngOnInit(): void {
    let id = localStorage.getItem('id')    
    this._hubConnection = new HubConnectionBuilder().withUrl('http://localhost:24454/notify').build();
    this._hubConnection.start()
      .then(() =>
        this.sendData())
      .catch(err => {
        console.log('Error while establishing the connection')
      });

    this._hubConnection.on('BroadcastMessage', (message) => {
      if(message.idReciever = id){
        if(message.type != 1){
          alert(message.notice)
        }
        console.log(message)
        this.signaldata.push(message);
        this.showimage = true;
      }
    })
    this.loadlogin()
    this.loaduser()

    document.addEventListener("DOMContentLoaded", function () {
      let menu = document.querySelector('.now-banner');

      let trangthai = menu?.classList.contains('add');
      window.addEventListener("scroll", function () {
        var x = pageYOffset;

        if (x >= 500) {
          // if(trangthai){              
          menu?.classList.remove('add')
          // }
        }
        else if (x < 500) {
          // if(!trangthai){              
          menu?.classList.add('add')
          // }
        }
      })
    })
  }


  showMessage() {
    this.showimage = false;
  }

  async sendData() {    
    var data = {
      Type: 1,
      Notice: "connect"
    }
    await fetch('http://localhost:24454/api/Message', {
      method: 'POST', // or 'PUT'
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then(data => {
        // console.log('Success:', data);
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }


  async loaduser() {
    let id = localStorage.getItem('id')
    await fetch(`http://localhost:24454/api/Users/${id}`)
      .then(response => response.json())
      .then(data => this.user = data);
  }

  loadlogin() {
    let status = localStorage.getItem('token')
    let eUser = document.querySelector('#user')
    let elogin = document.querySelector('#login')
    if (status === null) {
      eUser?.classList.toggle('hidden')
      elogin?.classList.toggle('hidden')
    }
  }

  logout() {
    localStorage.removeItem('token')

    let eUser = document.querySelector('#user')
    let elogin = document.querySelector('#login')

    eUser?.classList.toggle('hidden')
    elogin?.classList.toggle('hidden')

  }

  onchanges(e: any) {
    console.log(e);
    console.log(this.selectedValue);
  }

  clicktoggleuser() {
    let e = document.querySelector(".user-acc .dropdown-menu");
    e?.classList.toggle('show')
  }
}
