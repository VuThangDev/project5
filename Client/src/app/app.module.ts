import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
// import { MatSliderModule } from '@angular/material/slider';
import {MatSelectModule} from '@angular/material/select';
import { AgmCoreModule } from '@agm/core';
import {MatTabsModule} from '@angular/material/tabs';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {MatDialogModule} from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarHeaderComponent } from './users/component/sidebar-header/sidebar-header.component';
import { FooterComponent } from './users/component/footer/footer.component';
import { UserComponent } from './users/user/user.component';
import { HomeComponent } from './users/page/home/home.component';
import { BannerComponent } from './users/component/banner/banner.component';
import { ContainMainHomeComponent } from './users/component/contain-main-home/contain-main-home.component';
import { ListrestaurantComponent } from './users/component/listrestaurant/listrestaurant.component';
import { InfomationMainComponent } from './users/component/infomation-main/infomation-main.component';
import { ShopComponent } from './users/page/shop/shop.component';
import { MenuComponent } from './users/component/menu/menu.component';
import { ItemmenuComponent } from './users/component/menushop/itemmenu/itemmenu.component';
import { PayComponent } from './users/component/pay/pay.component';
import { RegisterDriverComponent } from './users/page/register-driver/register-driver.component';
import { BannerRegisterShopComponent } from './users/page/banner-register-shop/banner-register-shop.component';
import { RegisterShopComponent } from './users/page/register-shop/register-shop.component';
import { CheckzoneComponent } from './users/component/checkzone/checkzone.component';
import { SigncontractComponent } from './users/component/signcontract/signcontract.component';
import { BasicinfoComponent } from './users/component/basicinfo/basicinfo.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { FreshComponent } from './users/page/fresh/fresh.component';
import { DrinkComponent } from './users/page/drink/drink.component';
import { ListrestaurantFindComponent } from './users/page/listrestaurant-find/listrestaurant-find.component';
import { HistoryOrderComponent } from './users/page/history-order/history-order.component';
import { MerchantComponent } from './users/page/merchant/merchant.component';
import { QLDHComponent } from './users/page/qldh/qldh.component';
import { QLSPComponent } from './users/page/qlsp/qlsp.component';
import { ThanhtoanComponent } from './users/component/thanhtoan/thanhtoan.component';
import { LoginComponent } from './users/page/login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    SidebarHeaderComponent,
    FooterComponent,
    UserComponent,
    HomeComponent,
    BannerComponent,
    ContainMainHomeComponent,
    ListrestaurantComponent,
    InfomationMainComponent,
    ShopComponent,
    MenuComponent,
    ItemmenuComponent,
    PayComponent,
    RegisterDriverComponent,
    BannerRegisterShopComponent,
    RegisterShopComponent,
    CheckzoneComponent,
    SigncontractComponent,
    BasicinfoComponent,
    FreshComponent,
    DrinkComponent,
    ListrestaurantFindComponent,
    HistoryOrderComponent,
    MerchantComponent,
    QLDHComponent,
    QLSPComponent,
    ThanhtoanComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // MatSliderModule,
    MatSelectModule,
    AgmCoreModule.forRoot({
      apiKey: ''
    }),
    BrowserAnimationsModule,
    FormsModule,
    MatTabsModule,
    MatPaginatorModule,
    MatTableModule,
    MatDialogModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
