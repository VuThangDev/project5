﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using marketgo.Models;

namespace marketgo.Models
{
    public class ViewOrder
    {
        public Order orders { get; set; }
        public Shop shops { get; set; }
        //public OrderDetail orderDetails { get; set; }
        //public Menu menus { get; set; }
        //public MenuDetail menuDetails { get; set; }
    }
}
