﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace marketgo.Models
{
    public class Message
    {
        public int type { get; set; }

        public string notice { get; set; }

        public int idSender { get; set; }
        public int idReciever { get; set; }        
    }
}
