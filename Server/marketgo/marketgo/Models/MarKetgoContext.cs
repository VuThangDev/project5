﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace marketgo.Models
{
    public partial class MarKetgoContext : DbContext
    {
        public MarKetgoContext()
        {
        }

        public MarKetgoContext(DbContextOptions<MarKetgoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Admin> Admins { get; set; }
        public virtual DbSet<Menu> Menus { get; set; }
        public virtual DbSet<MenuDetail> MenuDetails { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<OpenTime> OpenTimes { get; set; }
        public virtual DbSet<OpenTimeDetail> OpenTimeDetails { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderDetail> OrderDetails { get; set; }
        public virtual DbSet<Registration> Registrations { get; set; }
        public virtual DbSet<RegistrationDriver> RegistrationDrivers { get; set; }
        public virtual DbSet<Shiper> Shipers { get; set; }
        public virtual DbSet<Shop> Shops { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Vote> Votes { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//                optionsBuilder.UseSqlServer("Server=PC\\VANTHANG;Database=MarKetgo;Trusted_Connection=True;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Vietnamese_100_CS_AS_KS_WS_SC_UTF8");

            modelBuilder.Entity<Admin>(entity =>
            {
                entity.HasKey(e => e.IdAdmin);

                entity.ToTable("Admin");

                entity.Property(e => e.Address)
                    .HasMaxLength(550)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.Avatar)
                    .HasMaxLength(650)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.CreateAt).HasColumnType("date");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(250)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.Name)
                    .HasMaxLength(250)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(250)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");
            });

            modelBuilder.Entity<Menu>(entity =>
            {
                entity.HasKey(e => e.IdMenu);

                entity.ToTable("Menu");

                entity.Property(e => e.Name)
                    .HasMaxLength(350)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.HasOne(d => d.IdShopNavigation)
                    .WithMany(p => p.Menus)
                    .HasForeignKey(d => d.IdShop)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Menu_Shops");
            });

            modelBuilder.Entity<MenuDetail>(entity =>
            {
                entity.HasKey(e => e.IdDetail);

                entity.ToTable("MenuDetail");

                entity.Property(e => e.Avatar)
                    .HasMaxLength(650)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.Describe)
                    .HasMaxLength(550)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.Name)
                    .HasMaxLength(250)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.HasOne(d => d.IdMenuNavigation)
                    .WithMany(p => p.MenuDetails)
                    .HasForeignKey(d => d.IdMenu)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_MenuDetail_Menu");
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.HasKey(e => e.IdNotification);

                entity.Property(e => e.Message)
                    .HasMaxLength(1000)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.Type).HasColumnName("type");
            });

            modelBuilder.Entity<OpenTime>(entity =>
            {
                entity.HasKey(e => e.IdOpen);

                entity.ToTable("OpenTime");

                entity.HasOne(d => d.IdShopNavigation)
                    .WithMany(p => p.OpenTimes)
                    .HasForeignKey(d => d.IdShop)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_OpenTime_Shops");
            });

            modelBuilder.Entity<OpenTimeDetail>(entity =>
            {
                entity.HasKey(e => e.IdOpen);

                entity.ToTable("OpenTimeDetail");

                entity.Property(e => e.Day).HasColumnName("day");

                entity.HasOne(d => d.IdOpenTimeNavigation)
                    .WithMany(p => p.OpenTimeDetails)
                    .HasForeignKey(d => d.IdOpenTime)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_OpenTimeDetail_OpenTime");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.HasKey(e => e.IdOrder);

                entity.Property(e => e.Mind).HasMaxLength(550);
            });

            modelBuilder.Entity<OrderDetail>(entity =>
            {
                entity.HasKey(e => e.IdDetail);

                entity.ToTable("OrderDetail");

                entity.HasOne(d => d.IdOrderNavigation)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.IdOrder)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_OrderDetail_Orders");
            });

            modelBuilder.Entity<Registration>(entity =>
            {
                entity.HasKey(e => e.IdRegistrationShop);

                entity.ToTable("Registration");

                entity.Property(e => e.Adress)
                    .HasMaxLength(350)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.Avatar)
                    .HasMaxLength(650)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.Email)
                    .HasMaxLength(250)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.FacadeImage)
                    .HasMaxLength(650)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.IdentitycardBackside)
                    .HasMaxLength(650)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.IdentitycardFront)
                    .HasMaxLength(650)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.NameSurrogate)
                    .HasMaxLength(250)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");
            });

            modelBuilder.Entity<RegistrationDriver>(entity =>
            {
                entity.ToTable("RegistrationDriver");

                entity.Property(e => e.Address)
                    .HasMaxLength(350)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.IndentityCard)
                    .HasMaxLength(650)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.Name)
                    .HasMaxLength(250)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.ReferralSource)
                    .HasMaxLength(650)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");
            });

            modelBuilder.Entity<Shiper>(entity =>
            {
                entity.HasKey(e => e.IdShiper);

                entity.Property(e => e.Address)
                    .HasMaxLength(250)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.Name)
                    .HasMaxLength(250)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.Phone)
                    .HasMaxLength(250)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.Shipers)
                    .HasForeignKey(d => d.IdUser)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Shipers_Users");
            });

            modelBuilder.Entity<Shop>(entity =>
            {
                entity.HasKey(e => e.IdShop);

                entity.Property(e => e.Address)
                    .HasMaxLength(550)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");
                entity.Property(e => e.NameSearch)
                    .HasMaxLength(200)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.Avatar)
                    .HasMaxLength(650)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.CreateAt).HasColumnType("date");

                entity.Property(e => e.Name)
                    .HasMaxLength(550)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.Shops)
                    .HasForeignKey(d => d.IdUser)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Shops_Users");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.IdUser);

                entity.Property(e => e.Address)
                    .HasMaxLength(250)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.Avatar)
                    .HasMaxLength(550)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.Email)
                    .HasMaxLength(150)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.Name)
                    .HasMaxLength(250)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");

                entity.Property(e => e.Password)
                    .HasMaxLength(150)
                    .UseCollation("Vietnamese_100_CS_AI_KS_WS_SC_UTF8");
            });

            modelBuilder.Entity<Vote>(entity =>
            {
                entity.HasKey(e => e.IdVote);

                entity.ToTable("Vote");

                entity.HasOne(d => d.IdShopNavigation)
                    .WithMany(p => p.Votes)
                    .HasForeignKey(d => d.IdShop)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Vote_Shops");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
