﻿using System;
using System.Collections.Generic;

#nullable disable

namespace marketgo.Models
{
    public partial class OpenTime
    {
        public OpenTime()
        {
            OpenTimeDetails = new HashSet<OpenTimeDetail>();
        }

        public int IdOpen { get; set; }
        public int? IdShop { get; set; }
        public int? Monday { get; set; }
        public int? Tuesday { get; set; }
        public int? Wednesday { get; set; }
        public int? Thursday { get; set; }
        public int? Friday { get; set; }
        public int? Saturday { get; set; }
        public int? Sunday { get; set; }

        public virtual Shop IdShopNavigation { get; set; }
        public virtual ICollection<OpenTimeDetail> OpenTimeDetails { get; set; }
    }
}
