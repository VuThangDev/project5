﻿using System;
using System.Collections.Generic;

#nullable disable

namespace marketgo.Models
{
    public partial class User
    {
        public User()
        {
            Shipers = new HashSet<Shiper>();
            Shops = new HashSet<Shop>();
        }

        public int IdUser { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int? Phone { get; set; }
        public string Address { get; set; }
        public string Avatar { get; set; }
        public int? Role { get; set; }
        public double? lat { get; set; }
        public double? lng { get; set; }

        public virtual ICollection<Shiper> Shipers { get; set; }
        public virtual ICollection<Shop> Shops { get; set; }
    }
}
