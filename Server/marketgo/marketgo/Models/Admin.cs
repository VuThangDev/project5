﻿using System;
using System.Collections.Generic;

#nullable disable

namespace marketgo.Models
{
    public partial class Admin
    {
        public int IdAdmin { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public int? Phone { get; set; }
        public string Avatar { get; set; }
        public DateTime? CreateAt { get; set; }
        public int? Role { get; set; }
    }
}
