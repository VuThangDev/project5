﻿using System;
using System.Collections.Generic;

#nullable disable

namespace marketgo.Models
{
    public partial class Vote
    {
        public int IdVote { get; set; }
        public int? IdShop { get; set; }
        public int? Star1 { get; set; }
        public int? Star2 { get; set; }
        public int? Star3 { get; set; }
        public int? Star4 { get; set; }
        public int? Star5 { get; set; }

        public virtual Shop IdShopNavigation { get; set; }
    }
}
