﻿using System;
using System.Collections.Generic;

#nullable disable

namespace marketgo.Models
{
    public partial class Notification
    {
        public int IdNotification { get; set; }
        public int? ReceiverId { get; set; }
        public string Message { get; set; }
        public int? Type { get; set; }
    }
}
