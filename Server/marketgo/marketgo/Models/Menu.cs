﻿using System;
using System.Collections.Generic;

#nullable disable

namespace marketgo.Models
{
    public partial class Menu
    {
        public Menu()
        {
            MenuDetails = new HashSet<MenuDetail>();
        }

        public int IdMenu { get; set; }
        public int? IdShop { get; set; }
        public string Name { get; set; }

        public virtual Shop IdShopNavigation { get; set; }
        public virtual ICollection<MenuDetail> MenuDetails { get; set; }
    }
}
