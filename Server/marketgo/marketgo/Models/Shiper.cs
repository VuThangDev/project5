﻿using System;
using System.Collections.Generic;

#nullable disable

namespace marketgo.Models
{
    public partial class Shiper
    {
        public int IdShiper { get; set; }
        public int? IdUser { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public int? Status { get; set; }

        public virtual User IdUserNavigation { get; set; }
    }
}
