﻿using System;
using System.Collections.Generic;

#nullable disable

namespace marketgo.Models
{
    public partial class OpenTimeDetail
    {
        public int IdOpen { get; set; }
        public int? IdOpenTime { get; set; }
        public int? Day { get; set; }
        public TimeSpan? Opentime { get; set; }
        public TimeSpan? Closetime { get; set; }

        public virtual OpenTime IdOpenTimeNavigation { get; set; }
    }
}
