﻿using System;
using System.Collections.Generic;

#nullable disable

namespace marketgo.Models
{
    public partial class Registration
    {
        public int IdRegistrationShop { get; set; }
        public int? NameShop { get; set; }
        public string Avatar { get; set; }
        public string Adress { get; set; }
        public string Email { get; set; }
        public int? Phone { get; set; }
        public string NameSurrogate { get; set; }
        public string IdentitycardFront { get; set; }
        public string IdentitycardBackside { get; set; }
        public int? Type { get; set; }
        public string FacadeImage { get; set; }
    }
}
