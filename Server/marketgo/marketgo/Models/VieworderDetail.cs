﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace marketgo.Models
{
    public class VieworderDetail
    {
        //public Order orders { get; set; }
        public OrderDetail orderDetails { get; set; }
        //public Menu menus { get; set; }
        public MenuDetail menuDetails { get; set; }
    }
}
