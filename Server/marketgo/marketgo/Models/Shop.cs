﻿using System;
using System.Collections.Generic;

#nullable disable

namespace marketgo.Models
{
    public partial class Shop
    {
        public Shop()
        {
            Menus = new HashSet<Menu>();
            OpenTimes = new HashSet<OpenTime>();
            Votes = new HashSet<Vote>();
        }

        public int IdShop { get; set; }
        public int? IdUser { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int? Phone { get; set; }
        public int? Type { get; set; }
        public string Avatar { get; set; }
        public double? Rate { get; set; }
        public int? TotalVote { get; set; }
        public DateTime? CreateAt { get; set; }
        public double? lat { get; set; }
        public double? lng { get; set; }
        public int? city { get; set; }
        public int? selling { get; set; }

        public string NameSearch { get; set; }
        public virtual User IdUserNavigation { get; set; }
        public virtual ICollection<Menu> Menus { get; set; }
        public virtual ICollection<OpenTime> OpenTimes { get; set; }
        public virtual ICollection<Vote> Votes { get; set; }
    }
}
