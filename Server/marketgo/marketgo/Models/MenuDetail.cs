﻿using System;
using System.Collections.Generic;

#nullable disable

namespace marketgo.Models
{
    public partial class MenuDetail
    {
        public int IdDetail { get; set; }
        public int? IdMenu { get; set; }
        public int? idshop { get; set; } 
        public string Name { get; set; }
        public string Avatar { get; set; }
        public string Describe { get; set; }
        public double? Price { get; set; }
        public int? Total { get; set; }
        public int? TotalFavorite { get; set; }

        public virtual Menu IdMenuNavigation { get; set; }
    }
}
