﻿using System;
using System.Collections.Generic;

#nullable disable

namespace marketgo.Models
{
    public partial class RegistrationDriver
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? Phone { get; set; }
        public string IndentityCard { get; set; }
        public string Address { get; set; }
        public string ReferralSource { get; set; }
    }
}
