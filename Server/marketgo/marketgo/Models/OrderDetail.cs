﻿using System;
using System.Collections.Generic;

#nullable disable

namespace marketgo.Models
{
    public partial class OrderDetail
    {
        public int IdDetail { get; set; }
        public int? IdOrder { get; set; }
        public int? IdMenu { get; set; }
        public int? IdMenuDetail { get; set; }
        public int? Quantity { get; set; }

        public virtual Order IdOrderNavigation { get; set; }
    }
}
