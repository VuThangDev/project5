﻿using System;
using System.Collections.Generic;

#nullable disable

namespace marketgo.Models
{
    public partial class Order
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int IdOrder { get; set; }
        public int? IdUser { get; set; }
        public int? IdShop { get; set; }
        public int? IdShiper { get; set; }
        public string Mind { get; set; }
        public int? Status { get; set; }
        public double? Total { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
