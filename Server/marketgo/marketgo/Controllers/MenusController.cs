﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using marketgo.Models;

namespace marketgo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenusController : ControllerBase
    {
        private readonly MarKetgoContext _context;

        public MenusController(MarKetgoContext context)
        {
            _context = context;
        }

        // GET: api/Menus
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Order>>> GetMenus()
        {
            return await _context.Orders.ToListAsync();
        }

        // GET: api/Menus/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Menu>> GetMenu(int id)
        {
            var menu = await _context.Menus.FindAsync(id);

            if (menu == null)
            {
                return NotFound();
            }

            return menu;
        }

        [Route("GetMenu/{iduser}")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViewOrder>>> GetMenuUser(int iduser)
        {

                    var result = from o in _context.Orders
                                 join q in _context.Shops on o.IdShop equals q.IdShop
                                 //join m in _context.Menus on o.Id equals m.IdMenu
                                 //join md in _context.MenuDetails on d.IdMenuDetail equals md.IdDetail
                                 where o.IdUser == iduser
                                 select new ViewOrder
                                 {
                                     orders = o,
                                     shops = q
                                     //orderDetails = d,
                                     //menus = m,
                                     //menuDetails = md
                                 };

            return await result.ToListAsync();
        }

        [Route("GetMenuUDetail/{idorder}")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VieworderDetail>>> GetMenuUserDetail(int idorder)
        {

            var result = from o in _context.Orders
                         join d in _context.OrderDetails on o.IdOrder equals d.IdOrder
                         join m in _context.Menus on d.IdMenu equals m.IdMenu
                         join md in _context.MenuDetails on d.IdMenuDetail equals md.IdDetail
                         where o.IdOrder == idorder
                         select new VieworderDetail
                         {
                             //orders = o,
                             orderDetails = d,
                             //menus = m,
                             menuDetails = md
                         };

            return await result.ToListAsync();
        }

        [Route("GetMenuShop/{idshop}")]
        [HttpGet] 
        public async Task<ActionResult<IEnumerable<Menu>>> GetShopsNear(int idshop)
        {
            return await _context.Menus.Where(x => x.IdShop == idshop).ToListAsync();
        }

        [Route("GetDetailMenu/{idshop}")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MenuDetail>>> GetDetailMenu(int idshop)
        {
            return await _context.MenuDetails.Where(x => x.idshop == idshop).ToListAsync();
        }

        // PUT: api/Menus/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMenu(int id, Menu menu)
        {
            if (id != menu.IdMenu)
            {
                return BadRequest();
            }

            _context.Entry(menu).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MenuExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Menus
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Menu>> PostMenu(Menu menu)
        {
            _context.Menus.Add(menu);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMenu", new { id = menu.IdMenu }, menu);
        }

        [HttpPost]
        [Route("postorder")]
        public async Task<ActionResult<IEnumerable<Order>>> Postorder(Order o)
        {
            _context.Orders.Add(o);
            await _context.SaveChangesAsync();


            return _context.Orders.ToList();
        }
        [HttpPost]
        [Route("postorderdetail")]
        public async Task<ActionResult<IEnumerable<OrderDetail>>> Postorderdetail(OrderDetail [] od)
        {
            for(int i=0; i< od.Length; i++)
            {
            _context.OrderDetails.Add(od[i]);
            };           
                await _context.SaveChangesAsync();


            return _context.OrderDetails.ToList();
        }

        // DELETE: api/Menus/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMenu(int id)
        {
            var menu = await _context.Menus.FindAsync(id);
            if (menu == null)
            {
                return NotFound();
            }

            _context.Menus.Remove(menu);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool MenuExists(int id)
        {
            return _context.Menus.Any(e => e.IdMenu == id);
        }
    }
}
