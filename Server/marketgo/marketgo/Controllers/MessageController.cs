﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using marketgo.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace marketgo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private IHubContext<NoticeHub, ITypeHubClient> _hubContext;

        public MessageController(IHubContext<NoticeHub, ITypeHubClient> hubContext)
        {
            _hubContext = hubContext;
        }

        public int[] Idsignal;

        // GET: api/<MessageController>
        //[HttpGet]
        //public string Get()
        //{
        //    string retMessage = string.Empty;
        //    var message = new Message() { type = "warning", notice = 1 + Guid.NewGuid().ToString() };
        //    try
        //    {
        //        _hubContext.Clients.All.BroadcastMessage(message);
        //        retMessage = "Success";
        //    }
        //    catch (Exception e)
        //    {
        //        retMessage = e.ToString();
        //    }
        //    return retMessage;
        //}

        // GET api/<MessageController>/5
        [HttpPost]
        public string Post(Message message)
        {
            string retMessage = string.Empty;
            //Idsignal[Idsignal.Length-1] = message.notice;
            try
            {
                _hubContext.Clients.All.BroadcastMessage(message);
                retMessage = "Success";
            }
            catch (Exception e)
            {
                retMessage = e.ToString();
            }
            return retMessage;
        }
        [Route("noticeorder")]
        [HttpPost]
        public string NoticeOrder(Message message)
        {
            string retMessage = string.Empty;
            //Idsignal[Idsignal.Length-1] = message.notice;
            try
            {
                _hubContext.Clients.All.BroadcastMessage(message);
                retMessage = "Success";
            }
            catch (Exception e)
            {
                retMessage = e.ToString();
            }
            return retMessage;
        }

        [Route("noticeorderreciever")]
        [HttpPost]
        public string NoticeOrderReciever(Message message)
        {
            string retMessage = string.Empty;
            //Idsignal[Idsignal.Length-1] = message.notice;
            try
            {
                _hubContext.Clients.All.BroadcastMessage(message);
                retMessage = "Success";
            }
            catch (Exception e)
            {
                retMessage = e.ToString();
            }
            return retMessage;
        }
    }
}
