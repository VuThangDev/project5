﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using marketgo.Models;
using GoogleMaps.LocationServices;

namespace marketgo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShopsController : ControllerBase
    {
        private readonly MarKetgoContext _context;

        public ShopsController(MarKetgoContext context)
        {
            _context = context;
        }

        // GET: api/Shops
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Shop>>> GetShops()
        {
            return await _context.Shops.ToListAsync();
        }
        
        [Route("GetShopsNear/{city}")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Shop>>> GetShopsNear(int city)
        {            
            return await _context.Shops.Where(x => x.city == city).Take(10).ToListAsync();
        }
        [HttpPost("GetShopsName")]        
        public async Task<ActionResult<IEnumerable<Shop>>> GetShopsName(paginator p)
        {            
            return await _context.Shops.Where(x=>x.NameSearch.Contains(p.namf)).Skip(p.index).Take(p.size).ToListAsync();
        }

        [HttpPost("GetslShopsName")]
        public async Task<ActionResult<IEnumerable<Shop>>> GetslShopsName(paginator p)
        {
            return await _context.Shops.Where(x => x.NameSearch.Contains(p.namf)).ToListAsync();
        }

        [Route("getordershop/{idshop}")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViewOrder>>> GetMenuUser(int idshop)
        {

            var result = from o in _context.Orders
                         join q in _context.Shops on o.IdShop equals q.IdShop
                         //join m in _context.Menus on o.Id equals m.IdMenu
                         //join md in _context.MenuDetails on d.IdMenuDetail equals md.IdDetail
                         where o.IdShop == idshop
                         select new ViewOrder
                         {
                             orders = o,
                             shops = q
                             //orderDetails = d,
                             //menus = m,
                             //menuDetails = md
                         };

            return await result.ToListAsync();
        }

        [Route("getordershopdetail/{idorder}")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VieworderDetail>>> GetMenuUserDetail(int idorder)
        {

            var result = from o in _context.Orders
                         join d in _context.OrderDetails on o.IdOrder equals d.IdOrder
                         join m in _context.Menus on d.IdMenu equals m.IdMenu
                         join md in _context.MenuDetails on d.IdMenuDetail equals md.IdDetail
                         where o.IdOrder == idorder
                         select new VieworderDetail
                         {
                             //orders = o,
                             orderDetails = d,
                             //menus = m,
                             menuDetails = md
                         };

            return await result.ToListAsync();
        }

        [Route("GetShopsSelling/{city}")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Shop>>> GetShopsSelling(int city)
        {

            var orderByDescendingResult = from s in _context.Shops
                                          where s.city == city
                                          orderby s.selling descending
                                          select s;
            return await orderByDescendingResult.Take(10).ToListAsync();
        }

        [Route("GetLocation/{address}")]
        [HttpGet]
        public  float Getlocation(string address)
        {
            var a = "Stavanger, Norway";

            var locationService = new GoogleLocationService();
            var point = locationService.GetLatLongFromAddress(a);

            var latitude = point.Latitude;
            var longitude = point.Longitude;

            return (float)latitude;
        }

        // GET: api/Shops/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Shop>> GetShop(int id)
        {
            var shop = await _context.Shops.FindAsync(id);

            if (shop == null)
            {
                return NotFound();
            }

            return shop;
        }

        // PUT: api/Shops/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<ActionResult<string>> Putorder(int id, Order o)
        {
            if (id != o.IdOrder)
            {
                return BadRequest();
            }

            _context.Entry(o).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShopExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            var result = "succsess";
            return result.ToString();
        }

        // POST: api/Shops
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Shop>> PostShop(Shop shop)
        {
            _context.Shops.Add(shop);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetShop", new { id = shop.IdShop }, shop);
        }

        // DELETE: api/Shops/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteShop(int id)
        {
            var shop = await _context.Shops.FindAsync(id);
            if (shop == null)
            {
                return NotFound();
            }

            _context.Shops.Remove(shop);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ShopExists(int id)
        {
            return _context.Shops.Any(e => e.IdShop == id);
        }
    }
}
