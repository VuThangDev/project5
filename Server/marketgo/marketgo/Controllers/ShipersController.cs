﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using marketgo.Models;

namespace marketgo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShipersController : ControllerBase
    {
        private readonly MarKetgoContext _context;

        public ShipersController(MarKetgoContext context)
        {
            _context = context;
        }

        // GET: api/Shipers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Shiper>>> GetShipers()
        {
            return await _context.Shipers.ToListAsync();
        }

        // GET: api/Shipers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Shiper>> GetShiper(int id)
        {
            var shiper = await _context.Shipers.FindAsync(id);

            if (shiper == null)
            {
                return NotFound();
            }

            return shiper;
        }

        // PUT: api/Shipers/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutShiper(int id, Shiper shiper)
        {
            if (id != shiper.IdShiper)
            {
                return BadRequest();
            }

            _context.Entry(shiper).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShiperExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Shipers
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Shiper>> PostShiper(Shiper shiper)
        {
            _context.Shipers.Add(shiper);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetShiper", new { id = shiper.IdShiper }, shiper);
        }

        // DELETE: api/Shipers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteShiper(int id)
        {
            var shiper = await _context.Shipers.FindAsync(id);
            if (shiper == null)
            {
                return NotFound();
            }

            _context.Shipers.Remove(shiper);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ShiperExists(int id)
        {
            return _context.Shipers.Any(e => e.IdShiper == id);
        }
    }
}
