﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using marketgo.Models;

namespace marketgo
{
    public interface ITypeHubClient
    {
        Task BroadcastMessage(Message message);
    }
}
